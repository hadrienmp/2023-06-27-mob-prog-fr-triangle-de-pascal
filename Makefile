test:
	npm test


format:
	npm run format


update:
	nix flake update
	direnv exec . \
		nix develop --command \
			npm update


.PHONY: test lint update
