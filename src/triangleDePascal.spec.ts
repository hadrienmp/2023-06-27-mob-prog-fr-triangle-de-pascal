/*
3 =>
1 // array
1 1 // array
1 2 1 // array
6 =>
1
1 1
1 2 1
1 3 3 1
1 4 6 4 1
1 5 10 10 5 1
1 6 15 20 15 6 1
*/

type Triangle = Ligne[]
type Ligne = number[]


function triangle(taille: number): Triangle {
  if (taille === 1) return [[1]];

  const partieHaute = triangle(taille - 1)
  return [
    ...partieHaute,
    ligneSuivante(partieHaute),
  ]
}

function ligneSuivante(triangle: Triangle): Ligne {
  const précédente = triangle[triangle.length - 1];
  return [0, ...précédente, 0]
    .map((value, index, tableau) => value + tableau[index + 1])
    .slice(0, -1);
}

describe("Triangle de Pascal", () => {
  it("Première ligne", () => {
    expect(triangle(1)).toEqual([
      [1],
    ])
  })

  it("Deuxième ligne", () => {
    expect(triangle(2)).toEqual([
      [1],
      [1, 1],
    ])
  })

  it("Troisième ligne", () => {
    expect(triangle(3)).toEqual([
      [1],
      [1, 1],
      [1, 2, 1],
    ])
  })

  it("Quatrième ligne", () => {
    expect(triangle(4)).toEqual([
      [1],
      [1, 1],
      [1, 2, 1],
      [1, 3, 3, 1],
    ])
  })

  it("Cinquième ligne", () => {
    expect(triangle(5)).toEqual([
      [1],
      [1, 1],
      [1, 2, 1],
      [1, 3, 3, 1],
      [1, 4, 6, 4, 1],
    ])
  })

  it("Sixième ligne", () => {
    expect(triangle(6)).toEqual([
      [1],
      [1, 1],
      [1, 2, 1],
      [1, 3, 3, 1],
      [1, 4, 6, 4, 1],
      [1, 5, 10, 10, 5, 1],
    ])
  })

  it("Septième ligne", () => {
    expect(triangle(7)).toEqual([
      [1],
      [1, 1],
      [1, 2, 1],
      [1, 3, 3, 1],
      [1, 4, 6, 4, 1],
      [1, 5, 10, 10, 5, 1],
      [1, 6, 15, 20, 15, 6, 1],
    ])
  })
})
